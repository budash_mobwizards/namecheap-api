<?php
require_once 'vendor/autoload.php';
require_once 'src/bootstrap.php';

use App\NamecheapApi;
use App\DNSHostUpdater;

/* Get Namecheap Apiuser credentials*/
function getConfigValues(){
	$data = file_get_contents('config.json');
	$res = json_decode($data, true);
	return $res;
}

$data = [];
$config = getConfigValues();
$domains = $config['domains'];

$namecheap = new NamecheapApi($config);
$dnsObj = new DNSHostUpdater();

foreach ($domains as $key => $domain) {
	// Fetch domain DNS info
	echo "Checking A record for domain : $domain\n";
	$dns_info = $dnsObj->getDomainDNSHosts($domain, $namecheap);
	$dns_info = $dns_info['CommandResponse']['DomainDNSGetHostsResult'];
	echo "\nFetch DNS Host JSON : \n";
	echo json_encode($dns_info)."\n";	
	
	// Exclude Domains that have no Host records
	if (!isset($dns_info['host'])) {
		echo "\nDNS Host NOT Found, continuing with next domain.\n";
		echo "\n############## \n";
		continue;
	}
	
	// Filter domains with required A record and ip,
	// returns host records in array used by update function
	$filter_host =  $dnsObj->filterHost($dns_info['host'], $config['originalIp'], $config['newIP']);
	if (is_null($filter_host)) {
		echo "Skipping for domain : $domain\n";
		echo "\n############## \n";
		continue;
	}

	// update A record ip
	echo "Updating A record ip. \n";
	$status = $dnsObj->updateDomainARecord($domain, $filter_host, $namecheap);
	echo "\n".json_encode($status['@attributes']);

	echo "\n############## \n";
}