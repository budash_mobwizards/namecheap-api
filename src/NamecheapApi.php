<?php

namespace App;

class NamecheapApi {

	private $username;
	private $apiKey;
	private $clientIp;
	private $sandbox;

	const SANDBOX_URL='https://api.sandbox.namecheap.com/xml.response';
	const LIVE_URL='https://api.namecheap.com/xml.response';

	public function __construct($config) {
		$this->username = $config['username'];
		$this->apiKey = $config['apiKey'];
		$this->clientIp = $config['clientIp'];
		$this->sandbox = $config['sandbox'];
	}

	/**
	* Function to send request to namecheap api
	*
	* @param [Array] parameter for api request
	* @return Array 
	* @author Bishal Udash
	**/
	public function request($postData) {
	    // Check if environment is sandbox or not
		$url = self::SANDBOX_URL;
		if (!$this->sandbox) {
			$url = self::LIVE_URL;
		}

		$postData['ApiUser'] = $this->username;
		$postData['UserName'] = $this->username;
		$postData['ApiKey'] = $this->apiKey;
		$postData['ClientIp'] = $this->clientIp;

	    // Updates url with all the required parameters
		$url = sprintf("%s?%s", $url, http_build_query($postData));
		echo "Sending GET request to URL: ".$url;
		echo "\n";

		$client = new \GuzzleHttp\Client();
		$response = $client->request('GET', $url);
		return $this->formatResponse($response->getBody());
	}


	/**
	 * Function to parse XML string into associative array
	 *
	 * @param XML string returned from api
	 * @return Array
	 * @author Bishal Udash
	 **/
	public function formatResponse($apiData){
	    // convert xml response to json
		$data = simplexml_load_string($apiData);
	    // convert json to associative array
		$res = json_decode(json_encode($data), true);
		return $res;
	}
}

?>
