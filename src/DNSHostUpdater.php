<?php 
namespace App;

/**
* Class responsible for modifying domain IP
*
* @param [Array] parameter for api request
* @return Array 
* @author Bishal Udash
**/
class DNSHostUpdater
{
	/**
	 * Fetch DNS host Details for fetched domains
	 *
	 * @return array
	 **/
	public function getDomainDNSHosts($domain, $namecheap){
		$data['Command'] = "namecheap.domains.dns.getHosts";

		$domain = explode(".",$domain);
		$data['SLD'] = $domain[0];
		$data['TLD'] = $domain[1];

		$res = $namecheap->request($data);
		return $res;
	}

	
	/**
	 * Filter out domains having required ip Address
	 *
	 * @return array
	 **/
	public function filterHost($host_record, $oldip, $newip)
	{
		$recorddata = [];
		$recordtypes = [];
		$recordIP = [];

		// Preapare data for update and filter
		// Check if there is only 1 or multiple host records and handle accordingly.
		if(count($host_record) == 1){
			$item = 1;
			$recorddata['HostName'.$item] = $host_record['@attributes']['Name'];
			$recorddata['RecordType'.$item] = $host_record['@attributes']['Type'];
			$recorddata['Address'.$item] = $host_record['@attributes']['Address'];
			$recorddata['TTL'.$item] = $host_record['@attributes']['TTL'];

			array_push($recordtypes, $host_record['@attributes']['Type']);
			array_push($recordIP, $host_record['@attributes']['Address']);
		}else{
			foreach ($host_record as $key => $record) {
				$item = $key +1;
				$recorddata['HostName'.$item] = $record['@attributes']['Name'];
				$recorddata['RecordType'.$item] = $record['@attributes']['Type'];
				$recorddata['Address'.$item] = $record['@attributes']['Address'];
				$recorddata['TTL'.$item] = $record['@attributes']['TTL'];

				array_push($recordtypes, $record['@attributes']['Type']);
				array_push($recordIP, $record['@attributes']['Address']);
			}
		}

		echo "\nFiltering Host records.\n";
		// If A Record does not exist for domain return null
		if (!in_array('A', $recordtypes)) {
			echo "Type A Record not found.\n";
			return null;
		}

		// If A Record IP does not match with reuireq IP return null
		if (!in_array($oldip, $recordIP)) {
			echo "Type A Record with IP : $oldip doesnot exist for domain.\n";
			return null;
		}
		
		echo "Type A Record with IP : $oldip found.\n";
		$recorddata = $this->modifyHostRecordIP($recorddata,$oldip, $newip );
		return $recorddata;
	}

	/**
	 * Modify the old IP to newer one that matches condition
	 *
	 * @return Array
	 * @author 
	 **/
	public function modifyHostRecordIP($data,$oldip, $newip)
	{
		echo "\nModifying old IP address to new for A Record.\n";
		print_r($data);
		echo "\n";
		$ip_update_key = array_search($oldip, $data);
		$data[$ip_update_key] = $newip;
		print_r($data);
		echo "\n";

		return $data;
	}
	/**
	 * Update A record IP for domains in namecheap.
	 *
	 * @return array
	 **/
	public function updateDomainARecord($domain, $data, $namecheap){
		$data['Command'] = "namecheap.domains.dns.setHosts";

		$domain = explode(".",$domain);
		$data['SLD'] = $domain[0];
		$data['TLD'] = $domain[1];

		$res = $namecheap->request($data);
		return $res;
	}
}