## Requirements

-Mobwizards needs to urgently migrate around 200 domains to another server.

-For this we would like to automate changing all our DNS records with the Namecheap API, which is our registrar. 

-All domains should be searched for type “A” records where the IP address is 94.46.163.139, and this IP should be replaced with the new server IP, 94.46.171.91.

-Please make sure that nothing else is changed. 

-Records other than A type should not change.

-Records with A type pointing to IPs other than 94.46.163.139 should not change.

-Subdomains or related wildcards should not be changed. 

-The tool does not need to have very clean code or a user interface for now, though in the future we will want to implement many other API operations and have it integrated in the BO with an easy to use interface, using Laravel/Angular.


## Installation
- Make sure you have composer. First clone repo , cd into repo and install dependencies (only one ie guzzle)

	```composer install```

- Make sure you run this code from whitelisted ip machine or if you want to run locally just add you ip to namecheap's whitelist.

- Update the config values in config.json as required
	- "username": Namecheap Api username,
	- "apiKey" : Namecheap Api key,
	* "clientIp" : white listed api,
	- "sandbox" : "true",  // Make this false if you dont wanto use sandbox
	- "originalIp":"94.46.163.139",  // Old IP 
	- "newIP":"94.46.171.91", // New IP,  replace old with this new ip
	- "domains": Array of domain names . Eg: 
			["budash.com", "mobwizards.net"	]

- Make sure you have PHP Cli installed, Verify by running command `php -a ` in you terminal, it it opes php shell you are good to go.
- Run the script by running below command inside terminal:

	``` php index.php ```
